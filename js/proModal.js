var proModal = {};
/*var defaultData = {
	'headerClass': 'sky-blue',
	'openingTime': 500,
	'openingType': 'show',
	'title': 'proModal',
	'content': 'Hi!',
	'zoom':true,
};*/
proModal.modal = function(data = {})
{
	data = config(data);
	var body = $('body');
	var proModalIndex = body.find('[id^="proModal"]').length + 1;
	body.append('<div style="z-index:'+(300+proModalIndex*2)+'" id="proModal'+proModalIndex+'" class="proModal-ready"><div class="proModal-content"><div class="proModal-header-class"></div><div class="proModal-header"><span class="proModal-title">Header word</span><span class="proModal-close">×</span>'+(data.zoom == true ? '<span class="proModal-zoom plus">+</span>' : '')+'</div><div class="proModal-line"></div><div class="proModal-body"></div><div class="proModal-line"></div><div class="proModal-footer" align="right"></div></div></div>');
	var modal = body.find('#proModal'+proModalIndex).find('.proModal-content');
	var footer = modal.find('.proModal-footer');
	data.buttons.forEach(function(item, key){
		footer.append('<button class="btn '+item.btnClass+'">'+(typeof item.btnIcon == 'undefined' || item.btnIcon == '' ? '' : '<i class="'+item.btnIcon+'"></i>')+' '+(typeof item.btnName == 'undefined' ||item. btnName == '' ? "BTN-"+(key+1) : item.btnName)+'</button>');
		footer.find('button:last').click(function(){
			item.onClick($this = modal);
		});
	});
	modal.parent('div').fadeIn(100);
	modal.setTitle(data.title);
	modal.setHeaderClass(data.headerClass = 'sky-blue');
	modal.open(data.openingType, data.openingTime);
	modal.find('.proModal-close').click(function(){
		$(this).close(data.openingType, data.openingTime, proModalIndex);
	});
	modal.find('.proModal-zoom').click(function(){
		$(this).zoom();
	});
	return modal;
}
$.prototype.setTitle = function(title)
{
	$(this).find('.proModal-header').find('span.proModal-title').html(title);
	return $(this);
};
$.prototype.setHeaderClass = function(className)
{
	$(this).find('.proModal-header-class').addClass(className);
	$(this).find('.proModal-header').find('span.proModal-close').addClass('proModal-close-'+className);
	$(this).find('.proModal-header').find('span.proModal-zoom').addClass('proModal-zoom-'+className);
	return $(this);
}
$.prototype.close = function(openingType = 'hide', openingTime = 500, index = 1)
{
	switch(openingType)
	{
		case 'show':
			$(this).parents('.proModal-content:eq(0)').hide(openingTime);
			$(this).parents('.proModal-content:eq(0)').parents('.proModal-ready:eq(0)').fadeOut(openingTime);
		break;
		case 'fade':
			$(this).parents('.proModal-content:eq(0)').fadeOut(openingTime);
			$(this).parents('.proModal-content:eq(0)').parents('.proModal-ready:eq(0)').fadeOut(openingTime);
		break;
		case 'slide':
			$(this).parents('.proModal-content:eq(0)').slideUp(openingTime);
			$(this).parents('.proModal-content:eq(0)').parents('.proModal-ready:eq(0)').fadeOut(openingTime);
		break;
		default:
			$(this).parents('.proModal-content:eq(0)').hide(openingTime);
			$(this).parents('.proModal-content:eq(0)').parents('.proModal-ready:eq(0)').fadeOut(openingTime);
		break;
	}
	var bu = $(this).parents('.proModal-content:eq(0)');
	setTimeout(function(){
		bu.parents('#proModal'+index).remove();
	},openingTime+10);
};
$.prototype.open = function(openingType = 'show', openingTime = 500)
{
	switch(openingType)
	{
		case 'show':
			$(this).show(openingTime);
		break;
		case 'fade':
			$(this).fadeIn(openingTime);
		break;
		case 'slide':
			$(this).slideDown(openingTime);
		break;
		default:
			$(this).show(openingTime);
		break;
	}
	return $(this);
}
$.prototype.zoom = function()
{
	if($(this).hasClass('plus'))
	{
		$(this).removeClass('plus').addClass('minus');
		$(this).text('-');
		$(this).parents('.proModal-content').addClass('zoom');
	}
	else if($(this).hasClass('minus'))
	{
		$(this).removeClass('minus').addClass('plus');
		$(this).text('+');
		$(this).parents('.proModal-content').removeClass('zoom');
	}
	return $(this).parents('.proModal-content');
}
function config(data = {})
{
	var defaultData = {
		headerClass: 'sky-blue',
		openingTime: 500,
		openingType: 'show',
		title: 'proModal',
		content: 'Hi!',
		zoom:true,
		buttons: [
			{
				btnName: 'OK',
				btnClass: 'sky-blue',
				btnIcon: '',
				onClick: function($this){
					
				},
			}
		]
	};
	if(typeof data.headerClass != 'undefined') defaultData.headerClass = data.headerClass;
	if(typeof data.zoom != 'undefined') defaultData.zoom = data.zoom;
	if(typeof data.openingTime != 'undefined') defaultData.openingTime = data.openingTime;
	if(typeof data.openingType != 'undefined') defaultData.openingType = data.openingType;
	if(typeof data.title != 'undefined' && data.title != '') defaultData.title = data.title;
	if(typeof data.content != 'undefined' && data.content != '') defaultData.content = data.content;
	if(typeof data.buttons == 'object') defaultData.buttons = data.buttons;
	return defaultData;
}
$.prototype.proModalClose = function()
{
	$(this).find('.proModal-close').click();
}